﻿using System;
using System.Collections.Generic;
using System.Text;
using Thesis.Graph;
using System.Linq;
using Thesis.Trees;

namespace Thesis.NegativeCycleDetection
{
    public class DirectedPathConsistency
    {
        private Dictionary<int, Node> Vertices { get; set; } = new Dictionary<int, Node>();
        private Dictionary<int, Edge> Edges { get; set; } = new Dictionary<int, Edge>();
        private int Counter { get; set; }

        public bool CheckConsistency(IEnumerable<INode> V, IEnumerable<IEdge> E, TreeNode root)
        {
            List<int> d = new List<int>();
            AddNodeToList(root, d);
            d.Reverse();

            return CheckConsistency(V, E, d);
        }

        private void AddNodeToList(TreeNode node, List<int> d)
        {
            d.Add(node.Id);
            foreach(var child in node.Children)
                AddNodeToList(child, d);
        }

        public bool CheckConsistency(IEnumerable<INode> V, IEnumerable<IEdge> E, List<int> d)
        {
            if (V.Count() != d.Count)
                throw new Exception("Number of vertices and ordering must have the same number of elements");

            LoadGraph(V, E);

            for (var k = V.Count() - 1; k >= 0; k--)
            {
                var v_k = Vertices[d[k]];
                var incomingEdges = v_k.GetIncomingEdges();
                var outgoingEdges = v_k.GetOutgoingEdges();

                foreach (var incomingEdge in incomingEdges)
                {
                    foreach (var outgoingEdge in outgoingEdges)
                    {
                        var v_i = incomingEdge.Node1;
                        var v_j = outgoingEdge.Node2;
                        if (v_i == v_j)
                            continue;


                        var e_ik = v_i.Edges.FirstOrDefault(e => e.Node1 == v_i && e.Node2 == v_k);
                        var e_jk = v_j.Edges.FirstOrDefault(e => e.Node1 == v_j && e.Node2 == v_k);
                        var e_ki = v_k.Edges.FirstOrDefault(e => e.Node1 == v_k && e.Node2 == v_i);
                        var e_kj = v_k.Edges.FirstOrDefault(e => e.Node1 == v_k && e.Node2 == v_j);

                        var e_ij = v_i.Edges.FirstOrDefault(e => e.Node1 == v_i && e.Node2 == v_j);
                        var e_ji = v_j.Edges.FirstOrDefault(e => e.Node1 == v_j && e.Node2 == v_i);

                        if (e_ij == null)
                        {
                            e_ij = new Edge() { Id = Counter++, Node1 = v_i, Node2 = v_j, Weight = int.MaxValue };
                            Edges.Add(e_ij.Id, e_ij);
                            v_i.Edges.Add(e_ij);
                            v_j.Edges.Add(e_ij);
                        }

                        if (e_ji == null)
                        {
                            e_ji = new Edge() { Id = Counter++, Node1 = v_j, Node2 = v_i, Weight = int.MaxValue };
                            Edges.Add(e_ji.Id, e_ji);
                            v_i.Edges.Add(e_ji);
                            v_j.Edges.Add(e_ji);
                        }


                        var w_ij = Math.Min(e_ij.Weight, e_ik != null && e_kj != null ? e_ik.Weight + e_kj.Weight : int.MaxValue);
                        var w_ji = Math.Min(e_ji.Weight, e_jk != null && e_ki != null ? e_jk.Weight + e_ki.Weight : int.MaxValue);

                        e_ij.Weight = w_ij;
                        e_ji.Weight = w_ji;

                        if (w_ij + w_ji < 0)
                            return false;
                    }
                }

                foreach (var edge in v_k.Edges)
                {
                    var other = edge.Other(v_k);
                    other.Edges.Remove(edge);
                }
            }

            return true;
        }

        private void LoadGraph(IEnumerable<INode> V, IEnumerable<IEdge> E)
        {
            // Make a copy of the input graph
            foreach (var node in V)
                Vertices[node.Id] = new Node() { Id = node.Id };

            foreach (var edge in E)
            {
                var node1 = Vertices[edge.Node1.Id];
                var node2 = Vertices[edge.Node2.Id];
                var newEdge = new Edge() { Id = edge.Id, Node1 = node1, Node2 = node2, Weight = edge.Weight };
                Edges[edge.Id] = newEdge;
                node1.Edges.Add(newEdge);
                node2.Edges.Add(newEdge);
            }

            Counter = E.Count();
        }
    }
}
