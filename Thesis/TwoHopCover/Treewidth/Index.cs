﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Thesis.TwoHopCover.Treewidth
{
    public class Index
    {
        private Dictionary<int, Dictionary<int, Entry>> Entries { get; set; } = new Dictionary<int, Dictionary<int, Entry>>();

        public void Query(out int length, int v1, int v2)
        {
            length = int.MaxValue;

            if (Entries.TryGetValue(v1, out var v1SubIndex) && Entries.TryGetValue(v2, out var v2SubIndex))
                foreach (var key in v1SubIndex.Keys)
                    if (v2SubIndex.TryGetValue(key, out var v2Entry))
                        length = Math.Min(length, v1SubIndex[key].Length + v2Entry.Length);
        }

        public void Query(out int length, out int[] path, int v1, int v2)
        {
            length = int.MaxValue;
            path = Array.Empty<int>();

            if (Entries.TryGetValue(v1, out var v1SubIndex) && Entries.TryGetValue(v2, out var v2SubIndex))
                foreach (var key in v1SubIndex.Keys)
                    if (v2SubIndex.TryGetValue(key, out var v2Entry))
                    {
                        var pathLength = v1SubIndex[key].Length + v2Entry.Length;

                        if (pathLength >= length)
                            continue;

                        length = pathLength;

                        var path1 = GetPath(v1, key);
                        var path2 = GetPath(key, v2);

                        path2.Add(v2);
                        path = path1.Union(path2).ToArray();
                    }
        }

        /// <summary>
        /// Gets path from v1 to v2. Does not include v2.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private List<int> GetPath(int v1, int v2, List<int> path = null)
        {
            path ??= new List<int>() { v1 };
            if (v1 == v2)
                return path;

            Entry entry;
            if (Entries[v1].ContainsKey(v2))
                entry = Entries[v1][v2];
            else
                entry = Entries[v2][v1];

            if (entry.Middle != -1)
            {
                path.Add(entry.Middle);
                GetPath(v1, entry.Middle, path);
                GetPath(entry.Middle, v2, path);
            }
            return path;
        }

        public void AddOrUpdate(int v1, int v2, int length, int middle)
        {
            if (!Entries.TryGetValue(v1, out var subIndex))
                Entries[v1] = subIndex = new Dictionary<int, Entry>();
            subIndex[v2] = new Entry() { Length = length, To = v2, Middle = middle };
        }
    }
}
