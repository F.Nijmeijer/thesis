﻿using System;
using System.Collections.Generic;
using System.Text;
using Thesis.Graph;
using System.Linq;
using Thesis.Trees;

namespace Thesis
{
    public static class Extensions
    {
        public static Node Other(this Edge edge, Node node)
        {
            return edge.Node1.Equals(node) ? edge.Node2 : edge.Node1;
        }

        public static List<Node> GetNodes(this TreeNode treeNode, Dictionary<int, Node> nodes, List<Node> list = null)
        {
            list ??= new List<Node>();

            list.Add(nodes[treeNode.Id]);
            foreach(var child in treeNode.Children)
                child.GetNodes(nodes, list);

            return list;
        }

        public static List<Edge> GetOutgoingEdges(this Node node) => node.Edges.Where(e => e.Node1 == node).ToList();
        public static List<Edge> GetIncomingEdges(this Node node) => node.Edges.Where(e => e.Node2 == node).ToList();

        public static List<int> GetEliminationOrder(this TreeNode root, List<int> list = null)
        {
            list ??= new List<int>();
            list.Add(root.Id);
            foreach (var child in root.Children)
                child.GetEliminationOrder(list);
            return list;
        }
    }
}
